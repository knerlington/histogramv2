package fragments;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * FragmentPagerAdapter subclass specifying which and how many fragments to use
 * Created by knerlington on 2015-12-09.
 */
public class MPagerAdapter extends FragmentPagerAdapter {
    private static final int NUM_COUNT = 2;
    public MPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                FragmentAbout first = new FragmentAbout();
                return first;
            case 1:
                FragmentStart second = new FragmentStart();
                return second;
        }

        return null;
    }

    @Override
    public int getCount() {
        return NUM_COUNT;
    }
    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0:
                return "About";
            case 1:
                return "Start";

        }
        return "";
    }
}
