package fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.knerlington.histogramv2.R;



/**Fragment highlighting certain obstacles I've been facing throughout the development of the app
 * and what the app itself does.
 * Created by knerlington on 2015-12-09.
 */
public class FragmentAbout extends Fragment {
    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        TextView breadText = (TextView)view.findViewById(R.id.textView_bread);
        Typeface breadFont = Typeface.createFromAsset(getActivity().getAssets(), "Quicksand-Regular.ttf");
        breadText.setTypeface(breadFont);
        return view;
    }

}
