package helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/** Singleton class in charge of subsampling images from file, extracting image features, saving thumbnails etc.
 */
public class ImageHandler {
    private static ImageHandler imageHandler; //singleton instance

    public FSHelper fsHelper; //custom file system helper used internally for simplicity

    public static ImageHandler getInstance(){
        //returns instance of this class thus acting as singleton
        if(imageHandler == null){
            imageHandler = new ImageHandler();

        }
        return imageHandler;
    }

    private ImageHandler(){
        //set as private so it can't be instantiated outside of the class
        fsHelper = new FSHelper();
    }

    /*saveBitmapToFile()
    * creates a FileOutputStream to a file with the supplied filename in the specified directory.
    * to this file a thumbnail made from a subsampled source file should be written as a PNG*/
    public File saveBitmapToFile(String dir, String filename, Bitmap bitmap){
        FileOutputStream out = null;
        File createdFile = null;
        try {
            out = new FileOutputStream(fsHelper.createFileInCustomDir(dir, filename));

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            File[] thumbs = fsHelper.getFilesInDir(dir); //after bitmap is saved get reference to all thumbs
            createdFile = thumbs[thumbs.length-1]; //with all thumbnail references the latest file can be returned
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return createdFile;
    }

    /*returns the sample size to use when subsampling an image file*/
    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /*
    * subsamples the image file as much as possible based on reqWidth and reqHeight
    * using the power of two from calculateInSampleSize()*/
    public Bitmap subsampleImageFromFile(File file, int reqWidth, int reqHeight){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bm = BitmapFactory.decodeFile(file.getPath(), options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;


        return BitmapFactory.decodeFile(file.getPath(), options);

    }

    /*Saves mean RGB value separately in a Vector
    *Pass sampled files/thumbnails here*/
    public Vector getRGBFromImage(Bitmap bitmap){
        //create bitmap and save pixel data in db
        Bitmap bm = bitmap;
        Vector v = new Vector();
        //get each pixel from bitmap
        for(int w = 0; w < bm.getWidth();w++){
            for(int h = 0; h < bm.getHeight();h++){
                //save color int as separate channel value (0-255)
                v.r += Color.red(bm.getPixel(w, h)); //red
                v.g += Color.green(bm.getPixel(w, h)); //green
                v.b += Color.blue(bm.getPixel(w, h)); //blue
            }
        }
        v.r/=bm.getWidth()*bm.getHeight();
        v.g/=bm.getWidth()*bm.getHeight();
        v.b/=bm.getWidth()*bm.getHeight();

        return v;
    }

    /*
    * returns a File[] with content from passed directory name
    * the directory has to be located in the dir used within getFilesInDir() in FSHelper
    * see FSHelper for details
    * */
    public File[] getFilesFromFolder(String folderName){
        return fsHelper.getFilesInDir(folderName);
    }

    /*data structure for rgb values*/
    public class Vector{
        public int r, g, b;
    }

}
