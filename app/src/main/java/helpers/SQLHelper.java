package helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.util.Log;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * SQL wrapper simplifying database creation and opening.
 * Since all images are indexed in a SQLite db I let my wrapper handle all the sorting of the images.
 * The sorting returns only the file paths to respective image in order to keep memory usage down to a minimum.
 */
public class SQLHelper extends SQLiteOpenHelper {
    //Database schema
    private static final String DATABASE_NAME = "images.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "thumbnails";
    private static final String TABLE_NAME_HSV = "hsv";
    private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, PATH TEXT, R TEXT,G TEXT,B TEXT)";
    private static final String CREATE_TABLE_HSV = "CREATE TABLE "+TABLE_NAME_HSV+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, PATH TEXT, H TEXT,S TEXT,V TEXT)";

    /*Constructor - Opens or creates the db if it doesn't exist
    * Location is provided and set to my custom public app directory during instantiation*/
    public SQLHelper(Context context, String dbPath) {
        super(context,dbPath+"/"+DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db){
        //do stuff
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE_HSV); //Stores the RGB mean values converted as HSV values
        Log.d("sql", "Table " + TABLE_NAME + ", " +TABLE_NAME_HSV +" was created.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    /*getAllRowsForColumn()
    * Returns list with all thumbnail paths found in database under passed column*/
    public ArrayList<String> getAllRowsForColumn(String column){
        SQLiteDatabase db;
        ArrayList<String> names = new ArrayList<>();
        try{
            db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(String.format("SELECT %S FROM %s", column, TABLE_NAME),null);
            while(cursor.moveToNext()){
                String name = cursor.getString(cursor.getColumnIndex(String.format("%s",column)));
                names.add(name);
            }
            cursor.close();
            db.close();
        }catch (Exception e){
            //
        }
        return names;
    }
    public ArrayList<String> getColumnFromSortedQuery(String column, Cursor sortedResult){
        ArrayList<String> list = new ArrayList<>();
        while(sortedResult.moveToNext()){
            String value = sortedResult.getString(sortedResult.getColumnIndex(column));
            list.add(value);
        }
        return list;
    }
    public ArrayList<String> getNamesFromPaths(ArrayList<String> pathList){
        SQLiteDatabase db;
        ArrayList<String> names = new ArrayList<>();
        Cursor result = null;
        try{
            db = this.getReadableDatabase();
            for(String s: pathList){
                String query = String.format("SELECT NAME FROM %s WHERE PATH='%s'",TABLE_NAME, s);
                result = db.rawQuery(query, null);
                while (result.moveToNext()){
                    String value = result.getString(result.getColumnIndex("NAME"));
                    names.add(value);
                }
            } db.close(); result.close();

        }catch (Exception e){
            //
        }
        return names;
    }

    /*sortImagesAgainstSelectedImage()
    * calculates the difference in RGB mean between selected image and all other thumbs.
    * this is done directly in the sql query.
    * result is sorted in ascending order.
    * I.E. the bigger the difference the less similar the images are.*/
    public ArrayList<String> sortImagesAgainstSelectedImage(String currentImage){
        Log.d("db", "sort method reached");
        SQLiteDatabase db;
        ArrayList<String> paths = new ArrayList<>();
        Cursor result = null;
        try{
            db = this.getWritableDatabase();
            Cursor current = db.rawQuery(String.format("SELECT * FROM %s WHERE NAME='%s'", TABLE_NAME, currentImage), null);
            Log.d("db", "current cursor count "+current.getCount());
            int red = 0, green = 0, blue = 0;
            while(current.moveToNext()){
                Log.d("db", "current name: "+current.getString(current.getColumnIndex("NAME")));
                red = Integer.parseInt(current.getString(current.getColumnIndex("R")));
                green = Integer.parseInt(current.getString(current.getColumnIndex("G")));
                blue= Integer.parseInt(current.getString(current.getColumnIndex("B")));
                Log.d("db", "red "+red+"green "+green+"blue "+blue);
            }



            String query = String.format("SELECT PATH,(abs(R - %d) + abs(G - %d) + abs(B - %d)) AS diff FROM "+TABLE_NAME+ " ORDER BY diff ASC", red, green, blue);
            result = db.rawQuery(query, null);
            Log.d("db", "result count " + result.getCount());
            while(result.moveToNext()){
                String path = result.getString(result.getColumnIndex("PATH"));
                paths.add(path);
            }

            current.close();
            result.close();
            db.close();

        }catch (Exception e){
            //
        }
        return paths;
    }
    public ArrayList<String> sortFromHSV(String currentImage){
        Log.d("db", "sortFromHSV reached");
        SQLiteDatabase db;
        ArrayList<String> paths = new ArrayList<>();
        Cursor result = null;
        try{
            db = this.getWritableDatabase();
            Cursor current = db.rawQuery(String.format("SELECT * FROM %s WHERE NAME='%s'", TABLE_NAME_HSV, currentImage), null);
            Log.d("db", "current cursor count "+current.getCount());
            float h = 0, s = 0, v = 0;
            while(current.moveToNext()){
                Log.d("db", "current name: "+current.getString(current.getColumnIndex("NAME")));
                h = Float.parseFloat(current.getString(current.getColumnIndex("H")));
                s = Float.parseFloat(current.getString(current.getColumnIndex("S")));
                v = Float.parseFloat(current.getString(current.getColumnIndex("V")));
                Log.d("db", "hue "+h+" saturation "+s+" value "+v);
            }



            String query = String.format("SELECT PATH,(abs(H - %f) + abs(S - %f) + abs(V - %f)) AS diff FROM "+TABLE_NAME_HSV+ " ORDER BY diff ASC", h, s, v);
            result = db.rawQuery(query, null);
            Log.d("db", "result count " + result.getCount());
            while(result.moveToNext()){
                String path = result.getString(result.getColumnIndex("PATH"));
                paths.add(path);
            }

            current.close();
            result.close();
            db.close();

        }catch (Exception e){
            //
        }
        return paths;
    }

    /*sortUsingSqrt()*/
    public LinkedHashMap<String, Double> sortUsingSqrt(String currentImage){
        Log.d("db", "sort method reached");
        SQLiteDatabase db;
        ArrayList<String> paths = new ArrayList<>();
        Cursor result = null;
        LinkedHashMap<String, Double> diffMap = new LinkedHashMap<>();
        try{
            db = this.getWritableDatabase();
            Cursor current = db.rawQuery(String.format("SELECT * FROM %s WHERE NAME='%s'", TABLE_NAME, currentImage), null);
            Log.d("db", "current cursor count "+current.getCount());
            int red = 0, green = 0, blue = 0;
            while(current.moveToNext()){
                Log.d("db", "current name: "+current.getString(current.getColumnIndex("NAME")));
                red = Integer.parseInt(current.getString(current.getColumnIndex("R")));
                green = Integer.parseInt(current.getString(current.getColumnIndex("G")));
                blue= Integer.parseInt(current.getString(current.getColumnIndex("B")));
                Log.d("db", "red "+red+"green "+green+"blue "+blue);
            }



            String query = String.format("SELECT PATH,(abs(R - %d)*(abs(R-%d)) + abs(G - %d)*(abs(G-%d)) + abs(B - %d))*(abs(B-%d)) AS diff FROM "+TABLE_NAME+ " ORDER BY diff ASC", red,red,green, green, blue,blue);
            result = db.rawQuery(query, null);
            Log.d("db", "result count " + result.getCount());
            //save diff values in hashmap with paths

            while(result.moveToNext()){
                String path = result.getString(result.getColumnIndex("PATH"));
                paths.add(path);

                //get sqrt of diff value
                double val = Double.valueOf(result.getString(result.getColumnIndex("diff")));
                double rootVal = Math.sqrt(val);
                diffMap.put(path, rootVal);
                Log.d("sort", "diff value: " + rootVal);
            }

            current.close();
            result.close();
            db.close();

        }catch (Exception e){
            //
        }
        return diffMap;
    }

    /*
    * Inserts data into the database created in the constructor
    * */
    public void insertData(String name, String path, String r, String g, String b){
        SQLiteDatabase db;
        try{
            db = this.getWritableDatabase();
            ContentValues content = new ContentValues();
            content.put("NAME", name);
            content.put("PATH", path);
            content.put("R", r);
            content.put("G", g);
            content.put("B", b);
            db.insert(TABLE_NAME, null, content);
            db.close();
        }catch (Exception e){
            Log.d("sqlhelper", "Failed to insert data into database");

        }



    }

    public void convertRGBtoHSV(String filename){
        Log.d("db", "convertTGBtoHSV reached");
        String rgbQuery = String.format("SELECT * FROM %s WHERE NAME='%s'",TABLE_NAME, filename);
        SQLiteDatabase db;
        try{
            db = this.getWritableDatabase();
            Cursor result = db.rawQuery(rgbQuery,null);
            //loop through result and convert each rgb val
            while(result.moveToNext()){
                int red = Integer.parseInt(result.getString(result.getColumnIndex("R")));
                int green = Integer.parseInt(result.getString(result.getColumnIndex("G")));
                int blue= Integer.parseInt(result.getString(result.getColumnIndex("B")));

                float[] hsv = new float[3];
                Color.RGBToHSV(red, green, blue, hsv);
                String path = result.getString(result.getColumnIndex("PATH"));
                String name = result.getString(result.getColumnIndex("NAME"));
                String h = Float.toString(hsv[0]), s = Float.toString(hsv[1]), v = Float.toString(hsv[2]);
                Log.d("convert", String.format("HUE: %s SATURATION: %s VALUE: %s",h,s,v));
                insertHSV(name, path, h, s, v);

            }
            result.close();
            db.close();


        }catch (Exception e){
            Log.d("sqlhelper", "Failed to insert or convert rgb to hsv");
        }


    }
    public void insertHSV(String name, String path, String h, String s, String v){
        SQLiteDatabase db;
        try{
            db = this.getWritableDatabase();
            ContentValues content = new ContentValues();
            content.put("NAME",name);
            content.put("PATH", path);
            content.put("H", h);
            content.put("S", s);
            content.put("V", v);
            db.insert(TABLE_NAME_HSV, null, content);
            db.close();
        }catch (Exception e){
            Log.d("sqlhelper", "Failed to insert data into database");

        }



    }
    /*
    * Checks for existing record in database
    * */
    public boolean hasRecord(String name){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        try{
            String query = "select NAME from " + TABLE_NAME+ " where " + "NAME" + "="  +"'" +name+"'";
            cursor = db.rawQuery(query,null);
            if(cursor.getCount() > 0){
                cursor.close();
                db.close();
                Log.d("sql", "record is in db.");
                return true;
            }
            cursor.close();
            db.close();
            Log.d("sql", "record is NOT in db.");
            return false;
        }catch (Exception e){

            return false;
        }



    }

   /* public ArrayList<String> getImageFeaturesFromRecord(String imageName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select R,G,B from " + TABLE_NAME+ " where " + "NAME" + "="  +"'" +imageName+"'";
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<String> features = new ArrayList<>();
        while(cursor.moveToNext()){
            features.add(cursor.getString(cursor.getColumnIndex("R")));
            features.add(cursor.getString(cursor.getColumnIndex("G")));
            features.add(cursor.getString(cursor.getColumnIndex("B")));
        }


        return features;
    }*/


}
