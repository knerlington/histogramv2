package com.knerlington.histogramv2;

import android.app.Activity;
import android.os.Bundle;

/**Credits showing my contact details*/
public class CreditsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
    }
}
