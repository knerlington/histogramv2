package com.knerlington.histogramv2;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import fragments.MPagerAdapter;

/**Welcome activity greeting the user telling him/her what the app is about to do.
 * */
public class WelcomeActivity extends AppCompatActivity {
    private FragmentPagerAdapter adapterViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
       
        ViewPager vpPager = (ViewPager) findViewById(R.id.pager);
        adapterViewPager= new MPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        PagerTabStrip pagerStrip = (PagerTabStrip)findViewById(R.id.pager_header);

        pagerStrip.setTextColor(Color.WHITE);
        pagerStrip.setTextSize(TypedValue.COMPLEX_UNIT_PX, 52);
        int cyanish = Integer.decode("#00DD93");
        pagerStrip.setTabIndicatorColor(cyanish);
    }
    public void startActivity(View v){
        switch (v.getId()){
            case R.id.button_ok:
                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(intent);
        }
    }

}

