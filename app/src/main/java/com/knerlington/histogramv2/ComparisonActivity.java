package com.knerlington.histogramv2;


import android.content.Intent;

import android.graphics.Bitmap;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import helpers.FSHelper;
import helpers.ImageHandler;
import helpers.SQLHelper;

/**Comparison activity triggering the sorting of the images between each other while showing the result in a GridView and a larger ImageView*/
public class ComparisonActivity extends AppCompatActivity {
    private ImageHandler imageHandler;
    private SQLHelper dbHelper;
    private FSHelper fsHelper;
    private GridView grid;
    private ArrayList<String> allFileNames, sortedFileNames;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comparison);
        grid = (GridView)findViewById(R.id.gridView_filtered);
        imageHandler = ImageHandler.getInstance();
        fsHelper = new FSHelper();
        dbHelper = new SQLHelper(this, fsHelper.getPublicExtStorageDir()+"/Histogram/database");
        allFileNames = dbHelper.getAllRowsForColumn("NAME");
        //set default main image
        setMainImage(allFileNames.get(0));
        sortedFileNames = dbHelper.sortFromHSV(allFileNames.get(0));
        feedSortedImages(sortedFileNames);
        setClickListener();

    }

    /**Sets the main image in the top ImageView*/
    public void setMainImage(String name){
        File[] files = imageHandler.getFilesFromFolder("Histogram");
        for(File f:files){
            Log.d("file", f.getPath());
            if(f.getName().equals(name)){
                Bitmap bm = imageHandler.subsampleImageFromFile(f, 300, 200);
                Log.d("bm", bm.toString());
                ImageView iv = (ImageView)findViewById(R.id.imageView_full);
                iv.setImageBitmap(bm);
            }
        }

    }

    /**Instructions on what to do when the user touches an item in the GridView
     * Swaps the main image to the one being touched and sorts the others against the new main image*/
    public void setClickListener(){
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Log.d("db", "filename for item in filtered grid " + sortedFileNames.get(position));
                ArrayList<String> names = dbHelper.getNamesFromPaths(sortedFileNames);
                setMainImage(names.get(position));

                //original sorting method using RGB values
                //sortedFileNames = dbHelper.sortImagesAgainstSelectedImage(names.get(position));

                //latest sorting method based on converted RGB to HSV values
                sortedFileNames = dbHelper.sortFromHSV(names.get(position));
                feedSortedImages(sortedFileNames);
            }
        });

    }
    /**DEPRECATED
     * Experimental way of sorting ordered hashmaps*/
    public ArrayList<String> sortHashMap(LinkedHashMap<String, Double> map){
        ArrayList<String> pathList = new ArrayList<>();
        double oldVal;
        for(LinkedHashMap.Entry<String, Double> e:map.entrySet()){
           pathList.add(e.getKey());
            Log.d("map", "value for entry: "+e.getValue());
        }

        return pathList;
    }

    /**Supplies new list of file paths for newly sorted images to the GridView*/
    public void feedSortedImages(ArrayList<String> sortedImages){
        GridView grid = (GridView)findViewById(R.id.gridView_filtered);
        grid.setAdapter(new ImageAdapter(sortedImages,this));
    }

    @Override
    public void onBackPressed() {
        //Do nothing
        //Not necessary for user to step back while app is running
        //If user adds more files to app dir user should re-index via menu button in action bar
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_comparison, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.action_restart:
                intent = new Intent(ComparisonActivity.this, WelcomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.action_credits:
                intent = new Intent(ComparisonActivity.this, CreditsActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
