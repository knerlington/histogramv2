package com.knerlington.histogramv2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;

import helpers.ImageHandler;

/**With supplied list of file paths this adapter will fetch a corresponding image for each item in the AdapterView used.
 * Created by knerlington on 2015-11-24.
 */
public class ImageAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> filePaths;

    public ImageAdapter(ArrayList<String> fileNames, Context context){
        this.context = context;
        this.filePaths = fileNames;
    }

    @Override
    public int getCount() {
        return this.filePaths.size();
    }

    @Override
    public Object getItem(int position) {
        return this.filePaths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.grid_item, null);
        }
        Bitmap bm = BitmapFactory.decodeFile(this.filePaths.get(position));
        ImageView v = (ImageView)convertView.findViewById(R.id.grid_item_image);
        v.setImageBitmap(bm);

        return convertView;
    }
}
