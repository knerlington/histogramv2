package com.knerlington.histogramv2;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import java.io.File;
import helpers.FSHelper;
import helpers.ImageHandler;
import helpers.SQLHelper;

/**Main activity performing all the heavy calculations on a parallel thread through the private class ThumbnailWorker found in this file. */
public class MainActivity extends AppCompatActivity {
    private ImageHandler imageHandler;
    private SQLHelper databaseHelper;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FSHelper fsHelper = new FSHelper();
         //init imageHandler
        imageHandler = ImageHandler.getInstance();
        imageHandler.fsHelper.createDirInPublicPrimaryExtRootStorage("Histogram");
        imageHandler.fsHelper.createDirInCustomDir("Histogram", "thumbs");
        imageHandler.fsHelper.createDirInCustomDir("Histogram", "database");
        databaseHelper = new SQLHelper(this, fsHelper.getPublicExtStorageDir()+"/Histogram/database");


        //run thumb worker
        //creates thumbnails for files not in db
        try{
            ThumbnailWorker task = new ThumbnailWorker();
            File[] sourceFiles = imageHandler.getFilesFromFolder("Histogram");
            task.execute(sourceFiles);
        }catch (Exception e){
            Log.d("main", "failed to start worker");
        }
    }

    /**Prompt shown to user to notify him/her that the app is ready to browse all the files and sort them against each other*/
    private void browseAlert() {
        //create alert dialog
        if (dialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Time to browse and compare").setTitle("All files indexed");
            builder.setPositiveButton("Browse", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    Intent intent = new Intent(MainActivity.this, ComparisonActivity.class);
                    startActivity(intent);
                }
            });

            dialog = builder.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
        }
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    /**AsyncTask performing all the heavy work asynchronously in order for the app to remain responsive -
     * and not to be flagged as unresponsive by the system.*/
    private class ThumbnailWorker extends AsyncTask<File, Integer, Boolean>{
        private ImageAdapter iAdapter;
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute(){
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "Loading files",
                    "Please wait!");
        }

        @Override
        protected void onProgressUpdate(Integer... values){
            Log.d("main", "bitmap from source folder wasn't in db. was created");
        }

        @Override
        protected Boolean doInBackground(File... params) {
            Log.d("main", "doInBack reached");
            Log.d("main", "size of params in task: " + params.length);
            for(File f:params) {
                if (!f.isDirectory()) {
                    //file isn't a directory, go to work
                    //check if filename is in db
                    if (!databaseHelper.hasRecord(f.getName()))
                    {
                        //filename doesn't exist in db, subsample image, save thumbnail file
                        Bitmap bm = imageHandler.subsampleImageFromFile(f, 128, 128);
                        File thumbnail = imageHandler.saveBitmapToFile("Histogram/thumbs", f.getName(), bm);
                        //extract features from thumbnail and index it in the db
                        ImageHandler.Vector v = imageHandler.getRGBFromImage(bm);
                        databaseHelper.insertData(thumbnail.getName(),thumbnail.getPath(), String.valueOf(v.r), String.valueOf(v.g), String.valueOf(v.b));
                        //convert RGB to HSV
                        databaseHelper.convertRGBtoHSV(thumbnail.getName());
                        //publish progress
                        publishProgress(0);

                    }
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute (Boolean result){
            //cancel the progress dialog started in onPreExecute and prompt user to start browsing
            progressDialog.cancel();
            browseAlert();


        }


    }
}
